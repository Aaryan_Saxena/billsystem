import React, { Component } from "react";
class BillingSystem extends Component {
  state = {
    products: [
      {
        code: "PEP221",
        prod: "Pepsi",
        price: 12,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "COK113",
        prod: "Coca Cola",
        price: 18,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "MIR646",
        prod: "Mirinda",
        price: 15,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "SLI874",
        prod: "Slice",
        price: 22,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "MIN654",
        prod: "Minute Maid",
        price: 25,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "APP652",
        prod: "Appy",
        price: 10,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "FRO085",
        prod: "Frooti",
        price: 30,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "REA546",
        prod: "Real",
        price: 24,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "DM5461",
        prod: "Dairy Milk",
        price: 40,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "KK6546",
        prod: "Kitkat",
        price: 15,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "PER5436",
        prod: "Perk",
        price: 8,
        instock: "No",
        category: "Chocolates",
      },
      {
        code: "FST241",
        prod: "5 Star",
        price: 25,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "NUT553",
        prod: "Nutties",
        price: 18,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "GEM006",
        prod: "Gems",
        price: 8,
        instock: "No",
        category: "Chocolates",
      },
      {
        code: "GD2991",
        prod: "Good Day",
        price: 25,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "PAG542",
        prod: "Parle G",
        price: 5,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "MON119",
        prod: "Monaco",
        price: 7,
        instock: "No",
        category: "Biscuits",
      },
      {
        code: "BOU291",
        prod: "Bourbon",
        price: 22,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "MAR951",
        prod: "MarieGold",
        price: 15,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "ORE188",
        prod: "Oreo",
        price: 30,
        instock: "No",
        category: "Biscuits",
      },
    ],
    copyProd: [],
    cart: [],
    active: false,
    view: -1,
    categories: ["Beverages", "Chocolates", "Biscuits"],
    stocks: ["Yes", "No"],
    priceRange: ["<10", "10-20", ">20"],
    filter: { category: "", stock: "", price: "" },
    filterIndex: -1,
    sortIndex: -1,
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.filter[input.name] = input.value;
    let { category, stock, price } = s1.filter;
    console.log(category, stock, price);
    if (category && !stock && !price) {
      let filter = s1.products.filter((pr) => pr.category === category);
      s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
    }
    if (!category && stock && !price) {
      let filter = s1.products.filter((pr) => pr.instock === stock);
      s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
    }
    if (!category && !stock && price) {
      if (price === "<10") {
        let filter = s1.products.filter((pr) => pr.price < 10);
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price === "10-20") {
        let filter = s1.products.filter((pr) => pr.price > 10 && pr.price < 20);
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price == ">20") {
        let filter = s1.products.filter((pr) => pr.price > 20);
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      }
    }
    if (category != "" && stock != "" && price != "") {
      if (price === "<10") {
        let filter = s1.products.filter(
          (pr) =>
            pr.category === category && pr.instock === stock && pr.price < 10
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price === "10-20") {
        let filter = s1.products.filter(
          (pr) =>
            pr.category === category &&
            pr.instock === stock &&
            pr.price > 10 &&
            pr.price < 20
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price == ">20") {
        let filter = s1.products.filter(
          (pr) =>
            pr.category === category && pr.instock === stock && pr.price > 20
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      }
    }
    if (category && stock && !price) {
      let filter = s1.products.filter(
        (pr) => pr.category === category && pr.instock === stock
      );
      s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
    }
    if (category && !stock && price) {
      if (price === "<10") {
        let filter = s1.products.filter(
          (pr) => pr.category === category && pr.price < 10
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price === "10-20") {
        let filter = s1.products.filter(
          (pr) => pr.category === category && pr.price > 10 && pr.price < 20
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price == ">20") {
        let filter = s1.products.filter(
          (pr) => pr.category === category && pr.price > 20
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      }
    }
    if (!category && stock && price) {
      if (price === "<10") {
        let filter = s1.products.filter(
          (pr) => pr.instock === stock && pr.price < 10
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price === "10-20") {
        let filter = s1.products.filter(
          (pr) => pr.instock === stock && pr.price > 10 && pr.price < 20
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      } else if (price == ">20") {
        let filter = s1.products.filter(
          (pr) => pr.instock === stock && pr.price > 20
        );
        s1.copyProd = filter.length === 0 ? [...s1.copyProd] : [...filter];
      }
    }
    s1.copyProd = this.sortFilter(s1.copyProd, s1.sortIndex);
    s1.filterIndex = 1;
    this.setState(s1);
  };
  sortFilter = (products, colNo) => {
    switch (colNo) {
      case 0:
        products.sort((p1, p2) => p1.code.localeCompare(p2.code));
        break;
      case 1:
        products.sort((p1, p2) => p1.prod.localeCompare(p2.prod));
        break;
      case 2:
        products.sort((p1, p2) => p1.category.localeCompare(p2.category));
        break;
      case 3:
        products.sort((p1, p2) => p1.price - p2.price);
        break;
      case 4:
        products.sort((p1, p2) => p1.instock.localeCompare(p2.instock));
        break;
      default:
        break;
    }
    return products;
  };
  handleView = () => {
    let s1 = { ...this.state };
    s1.view = 0;
    this.setState(s1);
  };
  sort = (colNo) => {
    let s1 = { ...this.state };
    console.log(s1.copyProd);
    let products = s1.filterIndex === 1 ? [...s1.copyProd] : [...s1.products];
    console.log(products);
    switch (colNo) {
      case 0:
        products.sort((p1, p2) => p1.code.localeCompare(p2.code));
        break;
      case 1:
        products.sort((p1, p2) => p1.prod.localeCompare(p2.prod));
        break;
      case 2:
        products.sort((p1, p2) => p1.category.localeCompare(p2.category));
        break;
      case 3:
        products.sort((p1, p2) => p1.price - p2.price);
        break;
      case 4:
        products.sort((p1, p2) => p1.instock.localeCompare(p2.instock));
        break;
      default:
        break;
    }
    s1.sortIndex = colNo;
    s1.copyProd = [...products];
    this.setState(s1);
  };
  makeProd = (arr) => {
    let s1 = { ...this.state };
    s1.copyProd = [...arr];
    this.setState(s1);
  };
  addCart = (index) => {
    let s1 = { ...this.state };
    let product =
      s1.filterIndex === 1 ? s1.copyProd[index] : s1.products[index];
    let findIndex = s1.cart.findIndex((pr) => pr.code === product.code);
    if (findIndex >= 0) s1.cart[findIndex].quantity++;
    else {
      product.quantity = 1;
      s1.cart.push(product);
    }
    this.setState(s1);
  };
  handleActive = (view) => (view === 0 ? "active" : "");
  render() {
    let {
      products,
      view,
      cart,
      categories,
      filter,
      priceRange,
      stocks,
      sortIndex,
      copyProd,
    } = this.state;
    const { category, stock, price } = filter;
    copyProd = copyProd.length === 0 ? this.makeProd(products) : [...copyProd];
    let qty =
      cart.length > 0
        ? cart.reduce((acc, curr) => (acc = acc + curr.quantity), 0)
        : "";
    let value =
      cart.length > 0
        ? cart.reduce(
            (acc, curr) => (acc = acc + curr.quantity * curr.price),
            0
          )
        : "";
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <a href="#" className="navbar-brand">
            BillingSystem
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a
                  href="#"
                  className={"nav-link " + this.handleActive(view)}
                  onClick={() => this.handleView()}
                >
                  New Bill
                </a>
              </li>
            </ul>
          </div>
        </nav>
        {view === 0 ? (
          <div className="container">
            <div className="jumbotron jumbotron-fluid bg-light p-0 mt-3">
              <h3>Details of Current Bill</h3>
              <p>
                {cart.length > 0
                  ? "Items:" +
                    cart.length +
                    ", Quantity:" +
                    qty +
                    "Amount:" +
                    value
                  : "Items:0, Quantity:0, Amount:0"}
              </p>
              {cart.map((pr, index) => {
                let { code, prod, price, quantity } = pr;
                let value = price * quantity;
                return (
                  <React.Fragment>
                    <div className="row p-0 m-0">
                      <div className="col-6 border">
                        {code} {prod} Price:{price} Quantity:{quantity} Value:
                        {value}
                      </div>
                      <div className="col-6  border p-0">
                        <button
                          className="btn btn-success btn-sm m-0"
                          onClick={() => this.addQuantity(index)}
                        >
                          +
                        </button>
                        <button
                          className="btn btn-warning btn-sm mx-2"
                          onClick={() => this.subQuantity(index)}
                        >
                          -
                        </button>
                        <button
                          className="btn btn-danger btn-sm mx-2"
                          onClick={() => this.remQuantity(index)}
                        >
                          x
                        </button>
                      </div>
                    </div>
                  </React.Fragment>
                );
              })}
              {cart.length === 0 ? (
                ""
              ) : (
                <button
                  className="btn btn-primary m-2"
                  onClick={() => this.closeCart()}
                >
                  Close Bill
                </button>
              )}
            </div>
            <h4 className="text-center">Product List</h4>
            <div className="row">
              <div className="col-3">
                <p className="font-weight-bold">Filter Products by :</p>
              </div>
              <div className="col-3">
                <div className="form-group">
                  <select
                    name="category"
                    className="form-control"
                    value={category}
                    onChange={this.handleChange}
                  >
                    <option value="">Select Category</option>
                    {categories.map((c1) => (
                      <option>{c1}</option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group">
                  <select
                    name="stock"
                    className="form-control"
                    value={stock}
                    onChange={this.handleChange}
                  >
                    <option value="">Select Stock</option>
                    {stocks.map((c1) => (
                      <option>{c1}</option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="col-3">
                <div className="form-group">
                  <select
                    name="price"
                    className="form-control"
                    value={price}
                    onChange={this.handleChange}
                  >
                    <option value="">Select Price Range</option>
                    {priceRange.map((c1) => (
                      <option>{c1}</option>
                    ))}
                  </select>
                </div>
              </div>
            </div>
            <div className="row bg-dark text-white font-weight-bold">
              <div className="col-2" onClick={() => this.sort(0)}>
                {sortIndex === 0 ? "Code(X)" : "Code"}
              </div>
              <div className="col-2" onClick={() => this.sort(1)}>
                {sortIndex === 1 ? "Product(X)" : "Product"}
              </div>
              <div className="col-2" onClick={() => this.sort(2)}>
                {sortIndex === 2 ? "Category(X)" : "Category"}
              </div>
              <div className="col-2" onClick={() => this.sort(3)}>
                {sortIndex === 3 ? "Price(X)" : "Price"}
              </div>
              <div className="col-2" onClick={() => this.sort(4)}>
                {sortIndex === 4 ? "In Stock(X)" : "In Stock"}
              </div>
              <div className="col-2"></div>
            </div>
            {copyProd.map((pr, index) => {
              let { code, prod, price, instock, category } = pr;
              return (
                <div className="row border text-seconadry">
                  <div className="col-2">{code}</div>
                  <div className="col-2">{prod}</div>
                  <div className="col-2">{category}</div>
                  <div className="col-2">{price}</div>
                  <div className="col-2">{instock}</div>
                  <div className="col-2">
                    <button
                      className="btn btn-secondary btn-sm"
                      onClick={() => this.addCart(index)}
                    >
                      Add To Bill
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
  addQuantity = (index) => {
    let s1 = { ...this.state };
    s1.cart[index].quantity++;
    this.setState(s1);
  };
  subQuantity = (index) => {
    let s1 = { ...this.state };
    s1.cart[index].quantity < 2
      ? s1.cart.splice(index, 1)
      : s1.cart[index].quantity--;
    this.setState(s1);
  };
  remQuantity = (index) => {
    let s1 = { ...this.state };
    s1.cart.splice(index, 1);
    this.setState(s1);
  };
  closeCart = () => {
    let s1 = { ...this.state };
    s1.copyProd = [...s1.products];
    s1.cart = [];
    s1.filterIndex = -1;
    s1.sortIndex = -1;
    s1.filter = { category: "", stock: "", price: "" };
    this.setState(s1);
  };
}
export default BillingSystem;
